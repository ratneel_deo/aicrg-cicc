/*
 * myGaPcx.cpp
 *
 *  Created on: 11/08/2009
 *      Author: rohit
 */
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>
//#include "benchmark.h"
#include "data.h"


//#include <realea/common/real.h>


//#include "functions7-11/F7-F11.h"
//#include "functions7-11/f7f11data.h"

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<ctype.h>


#include <stdio.h> // for printf()
#include <sys/time.h> // for clock_gettime()
#include <unistd.h> // for usleep()

time_t TicTime;
time_t TocTime;
using namespace::std;

typedef vector<double> Layer;
typedef vector<double> Nodes;
typedef vector<double> Frame;
typedef vector<int> Sizes;
typedef vector<vector<double> > Weight;
typedef vector<vector<double> > Data;




//-----------------------
const double alpha = 0.00001;
const double MaxErrorTollerance = 0.20;
const double MomentumRate = 0;
const double Beta = 0;
int row ;
int col;
int layer;
int r;
int x;
int y;

double weightdecay = 0.005;

//vector <vector<double> > a;

 //#include "objective.h"    //objective function
  //#include "random.h"       //random number generator
 #include "RandomNumber.h"       //random number generator


#define schwefel          // choose the function:schwefel, ellip

#define neuronlevel   //hiddenNeuron, weightlevel, neuronlevel


#define EPSILON 1e-50

#define MAXFUN 50000000  //upper bound for number of function evaluations

#define MINIMIZE 1      //set 1 to minimize and -1 to maximize
#define LIMIT 1e-20     //accuracy of best solution fitness desired
#define KIDS 2          //pool size of kids to be formed (use 2,3 or 4)
#define M 1             //M+2 is the number of parents participating in xover (use 1)
#define family 2        //number of parents to be replaced by good individuals(use 1 or 2)
#define sigma_zeta 0.1
#define sigma_eta 0.1   //variances used in PCX (best if fixed at these values)

//const int  PopSize = 100;

#define  NoSpeciesCons 300
//#define  NumV 3


#define NPSize KIDS + 2   //new pop size

#define RandParent M+2     //number of parents participating in PCX


#define MAXRUN 10      //number of runs each with different random initial population



double seed,basic_seed;



int RUN;
class Individual{

   //  friend class GeneticAlgorithmn;
   // friend class CoEvolution;
  //    friend class CombinedEvolution;
       public:

        Layer totalchrome;

        Layer Chrome;
        double Fitness;
        Layer BitChrome;

       public:
        Individual()
        {

        }
        void print();


      };

//****************************************

typedef vector<double> Nodes;
  class GeneticAlgorithmn :public virtual RandomNumber
  {
       public:

        int PopSize;
        int TotalPop;

        double arr[1000][100];

        vector <vector<double> > a;
        //vector<vector<Individual> > tempv; //-------------------

     	vector<Individual> Population;
     	vector<Individual> temporary;
     	//vector <vector<double> > a;


   	 Layer d_not;

     	Sizes TempIndex ;

     	vector<Individual> NewPop;
     	vector<Individual> All; //---------------

     	Sizes mom ;
     	Sizes list;

              int MaxGen;


       int NumVariable;
       //int TotalSize;

       double BestFit;
       int BestIndex;
       int NumEval;

       int  kids;

       public:
        GeneticAlgorithmn(int stringSize )
        {
     	   NumVariable = stringSize;
          NumEval=0;
           BestIndex = 0;

         }
        GeneticAlgorithmn()
       {
             BestIndex = 0;
        }



       double Fitness() {return BestFit;}

       double  RandomWeights(double limit);


       double  RandomAddition();

       void PrintPopulation();
       void PrintCopiedPopulation();
       void GetCopiedPopulation();


       int GenerateNewPCX(int pass,  double Mutation, int depth);

       double Objective(Layer x);

       void  InitilisePopulation(int popsize, double limit);

       void Evaluate();


       double  modu(double index[]);



       // calculates the inner product of two vectors

       double  innerprod(double Ind1[],double Ind2[]);

       double RandomParents();

       double MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &out3);

       double Noise();

       void  my_family();   //here a random family (1 or 2) of parents is created who would be replaced by good individuals

       void  find_parents() ;
       void  rep_parents() ;  //here the best (1 or 2) individuals replace the family of parents
       void sort();

   };

   //-------------------------------


double GeneticAlgorithmn::RandomWeights(double limit)
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
		  return  drand48()*limit;
       }

      if(chance ==1){

      return  drand48()*-limit;
     }

}

double GeneticAlgorithmn::RandomAddition()
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()% 100;
      return randomWeight*0.009;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100;
      return NegativeWeight*-0.009;
     }

}



void GeneticAlgorithmn::InitilisePopulation(int popsize, double limit)
    {

double x, y;

//double arr[2][2];

Individual Indi ;

	   NumEval=0;
           BestIndex = 0;
           PopSize = popsize;
           TotalPop = 8;



      for (int i = 0; i < PopSize; i++){
         TempIndex.push_back(0);
         mom.push_back(0);
          d_not.push_back(0); }

      for (int i = 0; i < NPSize; i++){
         list.push_back(0);}

 for(int row = 0; row < PopSize  ; row++) //chhange ---
        Population.push_back(Indi);

  for(int row = 0; row < PopSize  ; row++) {
   for(int col = 0; col < NumVariable ; col++){

      Population[row].Chrome.push_back(RandomWeights(limit));}
  }

   //copy the population and merge in a vector..
 //then resplit em.

 for(int row = 0; row < PopSize  ; row++){
        All.push_back(Indi);
        }

 for(int row = 0; row < PopSize  ; row++){
    for(int col = 0; col < NumVariable ; col++){
          //All[row].Chrome[col] = Population[row].Chrome[col];
          All[row].Chrome.push_back(Population[row].Chrome[col]);
          //All[row][col] = Population[row].Chrome[col];
          arr[row][col] = Population[row].Chrome[col];
        }
}

 for(int row = 0; row < NPSize  ; row++)
        NewPop.push_back(Indi);

  for(int row = 0; row < NPSize  ; row++) {
     for(int col = 0; col < NumVariable ; col++)
        NewPop[row].Chrome.push_back(0);

  }

  }

void GeneticAlgorithmn::Evaluate()
{
	// solutions are evaluated and best id is computed

	  Population[0].Fitness= Objective( Population[0].Chrome);
       BestFit = Population[0].Fitness;
       BestIndex = 0;

        for(int row = 0; row < PopSize  ; row++)
        {
          Population[row].Fitness= Objective( Population[row].Chrome);
          if ((MINIMIZE * BestFit) > (MINIMIZE * Population[row].Fitness))
          	{
        	  BestFit = Population[row].Fitness;
        	  BestIndex = row;
          	}
        }

}


void GeneticAlgorithmn::PrintPopulation()
{
	 for(int row = 0; row < PopSize   ; row++) { //pop/5
	   for(int col = 0; col < NumVariable ; col++)
	      cout<< Population[row].Chrome[col]<<" ";
	      cout<<endl;
	}



        //cout <<All[0].Chrome[0] << "  ";


	/*  for(int row = 0; row < PopSize  ; row++) //pop/5
		  cout<< "Member "<< row+1 << " fitness = "<<Population[row].Fitness<<endl;

	 cout<<" ---"<<endl;
	 cout<< "Best Fitness: "<<BestFit<<"  Best Index:  "<<BestIndex<<endl; */


		/*for(int row = 0; row < NPSize  ; row++) {
		   for(int col = 0; col < NumVariable ; col++)
		      cout<< NewPop[row].Chrome[col]<<" ";
		      cout<<endl;
		}*/

}


void GeneticAlgorithmn::PrintCopiedPopulation()
{


	//cout << "\n Copied _____________\n";

    for (int row=0; row<PopSize; row++){
        for (int col=0; col<NumVariable; col++){
            cout <<All[row].Chrome[col] <<"  ";// <<endl;

           // cout << arr[row][col <<"  ";

            }

        }

        for (int row=0; row<PopSize; row++){
            for (int col=0; col<NumVariable; col++){
            //cout <<All[row].Chrome[col] <<"  ";// <<endl;

            cout << arr[row][col] <<"  ";

            }

        }



}


void GeneticAlgorithmn::GetCopiedPopulation()
{
    for (int row=0; row<PopSize; row++){
        for (int col=0; col<NumVariable; col++){
            //All[row].Chrome[col];
            arr[row][col];
            }

            //return All;

        }
}

double GeneticAlgorithmn:: Objective(Layer x)
{

  return 0;
}
//------------------------------------------------------------------------

void GeneticAlgorithmn:: my_family()   //here a random family (1 or 2) of parents is created who would be replaced by good individuals
{
  int i,j,index;
  int swp;
  double u;

  for(i=0;i<PopSize;i++)
    mom[i]=i;

  for(i=0;i<family;i++)
    {
   //   u=randomperc();
  //    index=(u*(PopSize-i))+i;

	  index = (rand()%PopSize) +i;

	 // cout<<"is index  "<<index<<endl;
      if(index>(PopSize-1)) index=PopSize-1;
      swp=mom[index];
      mom[index]=mom[i];
      mom[i]=swp;
    }
}

void GeneticAlgorithmn::find_parents()   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

 my_family();
//cout<<kids<<endl;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
 	NewPop[kids+j].Chrome[i] = Population[mom[j]].Chrome[i];

      NewPop[kids+j].Fitness = Objective(NewPop[kids+j].Chrome);

    }
}



void GeneticAlgorithmn::rep_parents()   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
       Population[mom[j]].Chrome[i]=NewPop[list[j]].Chrome[i];

      Population[mom[j]].Fitness = Objective(Population[mom[j]].Chrome);

    }
}


void GeneticAlgorithmn::sort()

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(kids+family);i++) list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness < dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness > dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
}


//---------------------------------------------------------------------
double GeneticAlgorithmn::  modu(double index[])
{
  int i;
  double sum,modul;

  sum=0.0;
  for(i=0;i<NumVariable ;i++)
    sum+=(index[i]*index[i]);

  modul=sqrt(sum);
  return modul;
}

// calculates the inner product of two vectors
double GeneticAlgorithmn::  innerprod(double Ind1[],double Ind2[])
{
  int i;
  double sum;

  sum=0.0;

  for(i=0;i<NumVariable ;i++)
    sum+=(Ind1[i]*Ind2[i]);

  return sum;
}

int GeneticAlgorithmn::GenerateNewPCX(int pass,  double Mutation, int depth)
{
  int i,j,num,k;
  double Centroid[NumVariable];
  double tempvar,tempsum,D_not,dist;
  double tempar1[NumVariable];
  double tempar2[NumVariable];
  double D[RandParent];
  double d[NumVariable];
  double diff[RandParent][NumVariable];
  double temp1,temp2,temp3;
  int temp;

  for(i=0;i<NumVariable;i++)
    Centroid[i]=0.0;

  // centroid is calculated here
  for(i=0;i<NumVariable;i++)
    {
      for(j=0;j<RandParent;j++)
	Centroid[i]+=Population[TempIndex[j]].Chrome[i];

      Centroid[i]/=RandParent;

  //cout<<Centroid[i]<<" --- "<<RandParent<<"  ";
          // if(isnan(Centroid[i])) return 0;
    }
  //cout<<endl;

  // calculate the distace (d) from centroid to the index parent arr1[0]
  // also distance (diff) between index and other parents are computed
  for(j=1;j<RandParent;j++)
    {
      for(i=0;i<NumVariable;i++)
	{
	  if(j == 1)
	    d[i]=Centroid[i]-Population[TempIndex[0]].Chrome[i];
	  diff[j][i]=Population[TempIndex[j]].Chrome[i]-Population[TempIndex[0]].Chrome[i];
	}
      if (modu(diff[j]) < EPSILON)
	{
	  cout<< "RUN Points are very close to each other. Quitting this run   " <<endl;

	  return (0);
	}

 if (isnan(diff[j][i])  )
	{
	  cout<< "`diff nan   " <<endl;
             diff[j][i] = 1;
	  return (0);
	}


    }
  dist=modu(d); // modu calculates the magnitude of the vector

  if (dist < EPSILON)
    {
	  cout<< "RUN Points are very close to each other. Quitting this run    " <<endl;

      return (0);
    }

  // orthogonal directions are computed (see the paper)
  for(i=1;i<RandParent;i++)
    {
      temp1=innerprod(diff[i],d);
      if((modu(diff[i])*dist) == 0){
       cout<<" division by zero: part 1"<<endl;
         temp2=temp1/(1);
        }
      else{
      temp2=temp1/(modu(diff[i])*dist);}

      temp3=1.0-pow(temp2,2.0);
      D[i]=modu(diff[i])*sqrt(temp3);
    }

  D_not=0;
  for(i=1;i<RandParent;i++)
    D_not+=D[i];

  D_not/=(RandParent-1); //this is the average of the perpendicular distances from all other parents (minus the index parent) to the index vector

  // Next few steps compute the child, by starting with a random vector
  for(j=0;j<NumVariable;j++)
    {
      tempar1[j]=noise(0.0,(D_not*sigma_eta));
      //tempar1[j] = Noise();
      tempar2[j]=tempar1[j];
    }

  for(j=0;j<NumVariable;j++)
    {
      if ( pow(dist,2.0)==0){
     cout<<" division by zero: part 2"<<endl;
       tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/1);
   }
      else
      tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/pow(dist,2.0));
    }

  for(j=0;j<NumVariable;j++)
    tempar1[j]=tempar2[j];

  for(k=0;k<NumVariable;k++)
    NewPop[pass].Chrome[k]=Population[TempIndex[0]].Chrome[k]+tempar1[k];

 tempvar=noise(0.0,(sigma_zeta));

 // tempvar =Noise();


  for(k=0;k<NumVariable;k++){
      NewPop[pass].Chrome[k] += (tempvar*d[k]);

}

  double random = rand()%10;

Layer Chrome(NumVariable);

for(k=0;k<NumVariable;k++){
if(!isnan(NewPop[pass].Chrome[k] )){
 Chrome[k]=NewPop[pass].Chrome[k] ;
}
else
  NewPop[pass].Chrome[k] =  RandomAddition();

   }




  return (1);
}




//------------------------------------------------------------------------
double GeneticAlgorithmn::  RandomParents()
{

	int i,j,index;
	  int swp;
	  double u;
	  int delta;

	  for(i=0;i<PopSize;i++)
	    TempIndex[i]=i;

	  swp=TempIndex[0];
	  TempIndex[0]=TempIndex[BestIndex];  // best is always included as a parent and is the index parent
	                       // this can be changed for solving a generic problem
	 TempIndex[BestIndex]=swp;

	  for(i=1;i<RandParent;i++)  // shuffle the other parents
	    {
	    //u=randomperc();
	    index=(rand()%PopSize)+i;

	    if(index>(PopSize-1)) index=PopSize-1;
	    swp=TempIndex[index];
	    TempIndex[index]=TempIndex[i];
	    TempIndex[i]=swp;
	    }


}


void TIC( void )
{ TicTime = time(NULL); }

void TOC( void )
{ TocTime = time(NULL); }

double StopwatchTimeInSeconds()
{ return difftime(TocTime,TicTime); }



double GeneticAlgorithmn:: MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &out3 )
{
	  TIC();
	double tempfit =0;
    int count =0;
    int tag;
      kids = KIDS;

    int gen = MAXFUN/kids;
gen = 300000;
    basic_seed=0.4122;

        seed=basic_seed+(1.0-basic_seed)*(double)(RUN-1)/(double)MAXRUN;
        if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");
          randomize(seed);

	InitilisePopulation(100, 5);
    Evaluate();

    tempfit=Population[BestIndex].Fitness;

   for(count=1;((count<=gen)&&(tempfit>=LIMIT));count++)
   	{
    //   cout<<count<<endl;



	    RandomParents();           //random array of parents to do PCX is formed



	    for(int i=0;i<kids;i++)
	   	     {
	   	    if (tag == 0) break;
	   	     }
	   	   if (tag == 0) break;
	   	// PrintPopulation();

	        find_parents();  // form a pool from which a solution is to be
	   	                           //   replaced by the created child

	   		   sort();          // sort the kids+parents by fitness

	   		   rep_parents();   // a chosen parent is replaced by the child

	   		   //finding the best in the population
	   			  BestIndex=0;
	   			  tempfit=Population[0].Fitness;
	   			  cout<<tempfit<<" is temp fit"<<endl;
	   			  for(int i=1;i<PopSize;i++)
	   			    if((MINIMIZE * Population[i].Fitness) < (MINIMIZE * tempfit))
	   			      {
	   				tempfit=Population[i].Fitness;
	   				BestIndex=i;

	   			//	cout<<"yees  "<<BestIndex<<endl;
	   			      }

	   			  // print out results after every 100 generations
	   			    if (((count%100)==0) || (tempfit <= LIMIT))
	   			   out1<<count*kids<<"    "<<tempfit<<endl;

	   			 cout<<count<<" --------"<<tempfit<<endl;

	   			 		    		 //for(int i=0;i<	NumVariable;i++)
	   			 		    			// cout<<Population[BestIndex].Chrome[i]<<" ";
	   			 		    		 //cout<<endl;
    	}

   TOC();
      //  cout<<" Run Number: "<<RUN<<endl;
  // out2<<"Best solution obtained after X function evaluations:"<<count*kids<<" "<<NumEval<<endl;
out2<<RUN<<"   ";
        for(int i=0;i<NumVariable;i++)
  	 out2<<Population[BestIndex].Chrome[i]<<"      ";
        out2<<"---->  "<< tempfit<<"     "<<count*kids<<"      "<<StopwatchTimeInSeconds()<<endl;

        //out2<<"Fitness of this best solution:"<<tempfit<<endl;

        cout<<"Best solution obtained after X function evaluations:"<<count*kids<<" "<<NumEval<<endl;

              for(int i=0;i<NumVariable;i++)
            	  cout<<Population[BestIndex].Chrome[i]<<" ";
              cout<<endl;

              cout<<"Fitness of this best solution:"<<tempfit<<" "<<StopwatchTimeInSeconds()<<endl;


 	//   PrintPopulation();

     // }//run

}
//--------------------------------------------------------------------------------------------


typedef vector<GeneticAlgorithmn> GAvector;

class Table{

	//  friend class  CoEvolution ;
    //  friend class CombinedEvolution;
	    public:

       //double   SingleSp[100];
       Layer   SingleSp ;
       Layer AllSp;



};

typedef vector<Table> TableVector;

class CoEvolution :      public   GeneticAlgorithmn  ,public virtual RandomNumber
{



public:

      int  NoSpecies;
    GAvector Species ;

   // int NoSpecies2; //------------------------------
   // GAvector Species2; //---------------------------
   // GeneticAlgorithmn SingleGA;
//    Table  TableSp[NoSpeciesCons];

    TableVector  TableSp;
    TableVector test; //------

int PopSize;
int TotalPop; //----------------------


    vector<bool> NotConverged;
       Sizes SpeciesSize;

      // Sizes SpeciesSize2; //---------------------

       Layer   Individual;

       Layer Individual2; //-----------------
        vector <vector<double> > comb;

        //double chunk [][50][50];
        //double chunk [TotalSize/SpeciesSize[0]][PopSize][SpeciesSize[0]];

       Layer  BestIndividual;
      double bestglobalfit;
      Data TempTable;
int TotalEval;
     int TotalSize;
int ProbNum;
     int SingleGAsize;
     double Train;
          double Test;
     int kid;

      // public:
      // CoEvolution(int Size){
//
    //	   SpeciesSize = Size;
   //    }

    CoEvolution(){

    }

       void   MainProcedure(bool bp,   int RUN, ofstream &out1, ofstream &out2, ofstream &out3, double mutation,int depth );


     void InitialiseBoth();

     void Merge();

     void  InitializeSpecies(int popsize, double limit );
     void  EvaluateSpecies();
     void    GetBestTable(int sp);
     void PrintSpecies();

     void  PrintCopy(); //------------------------


     void   Join();
     double    ObjectiveFunc(Layer x);
     void Print();

     void   sort(int s );

     void  find_parents(int s);

     void EvalNewPop(int pass, int s );

     void  rep_parents(int s );

     void   EvolveSubPopulations(int repetitions,double h ,double mutation,int depth, ofstream &out2);

     void  MergePrint();

       };

void    CoEvolution:: InitializeSpecies(int popsize, double limit)
{

          PopSize = popsize;
         //Individual Indi;

         double a[1000][100];
        // CoEvolution Indi;

           GAvector SpeciesP(NoSpecies);
          Species = SpeciesP;

	 for( int Sp = 0; Sp < NoSpecies; Sp++){
	 	     NotConverged.push_back(false);
	 	     }

	   basic_seed=0.4122;
		   RUN =2;

		   seed=basic_seed+(1.0-basic_seed)*(double)((RUN)-1)/(double)MAXRUN;
		 			    	            if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");

			    			for( int s =0; s < NoSpecies; s++){

			    			Species[s].randomize(seed);

			    			}

			//   SingleGA.NumVariable = SingleGAsize;
		//	  SingleGA.InitilisePopulation();



	 TotalSize = 0;
		for( int row = 0; row< NoSpecies ; row++)
			TotalSize+= SpeciesSize[row];

		for( int row = 0; row< TotalSize ; row++)
		        Individual.push_back(0);
		        //Individual2.push_back(vector<double> a)


        TotalPop = PopSize * NoSpecies;
        //--------------------

        for (int i=0; i<PopSize; i++){


            vector<double> row;
            comb.push_back(row);

            }




	 for( int s =0; s < NoSpecies; s++){

	 	Species[s].NumVariable= SpeciesSize[s];
		  Species[s].InitilisePopulation(popsize, limit);
     }



	        TableSp.resize(NoSpecies);

             for( int row = 0; row< NoSpecies ; row++)
	            for( int col = 0; col < SpeciesSize[row]; col++)
	         	   TableSp[row].SingleSp.push_back(0);

//cout << "total Pop : " << TotalPop <<endl;
//cout << "total size : " << TotalSize <<endl;


/*
// copy ----------------------------------------------------------------------------------------------------
                        for (int sp=0; sp < NoSpecies; sp++){

      	                 for( int row = 0; row< PopSize ; row++) {

                            for( int col = 0; col < SpeciesSize[sp] ; col++) {   //totalsize

                                //comb[row][col] = Species[sp].Population[row].Chrome[col];
                                comb[row].push_back(Species[sp].Population[row].Chrome[col]);

                                //count++;
                            }


                        }

            }




cout << " \n \ntest copied [1][7] -> " << comb[1][7] << endl;

                    for( int row = 0; row< PopSize ; row++) {

                            for( int col = 0; col < TotalSize ; col++) {   //totalsize

                                //comb[row][col] = Species[sp].Population[row].Chrome[col];

                                cout << comb[row][col] << "  ";


                            }

                           //sp++;
                        }

//-----------------------------------------------------------------------------------------------------------------------

*/
/*
//Resplit and print------------------------------------------------------------------------------------------------------

cout << "\n\n Resplit ---- \n";

int c = 2;

double chunk [TotalSize/c][PopSize][c];

    for (int row = 0; row < PopSize; row++){

        for (int col=0; col<TotalSize; col++){
            chunk[col/c][row][col%c] = comb[row][col];

            }

        }



for (int x=0; x<TotalSize/c; x++){
    for (int row = 0; row < PopSize; row++){

        for (int k=0; k<c; k++){
            cout << chunk[x][row][k] ;//= comb[row][col];

            }
            cout <<endl;
        }
        cout <<endl<<endl;
    }

//------------------------------------------------------------------------------------------------------------------
*/

}


void CoEvolution::InitialiseBoth(){

	 TotalSize = 0;
		for( int row = 0; row< NoSpecies ; row++)
			TotalSize+= SpeciesSize[row];

// copy ----------------------------------------------------------------------------------------------------
                        for (int sp=0; sp < NoSpecies; sp++){

      	                 for( int row = 0; row< PopSize ; row++) {

                            for( int col = 0; col < SpeciesSize[sp] ; col++) {   //totalsize

                                //comb[row][col] = Species[sp].Population[row].Chrome[col];
                                comb[row].push_back(Species[sp].Population[row].Chrome[col]);

                                //count++;
                            }


                        }

            }




//cout << " \n \ntest copied [1][7] -> " << comb[0][0] << endl;

                    for( int row = 0; row< PopSize ; row++) {

                        for( int col = 0; col < TotalSize ; col++) {   //totalsize

                                cout << comb[row][col];
                               cout << "test";


                            }

                           //sp++;
                        }

//-----------------------------------------------------------------------------------------------------------------------
/*
cout << "\n\n Resplit ----  " << SpeciesSize[0] <<endl;

//int c = 2;


double chunk [TotalSize/SpeciesSize[0]][PopSize][SpeciesSize[0]];

for (int sp=0; sp < NoSpecies; sp++){
    for (int row = 0; row < PopSize; row++){

        for (int col=0; col<TotalSize; col++){
            chunk[col/SpeciesSize[sp]][row][col%SpeciesSize[sp]] = comb[row][col];

            }

        }
 }


for (int x=0; x<TotalSize/SpeciesSize[0]; x++){
    for (int row = 0; row < PopSize; row++){

        for (int k=0; k< SpeciesSize[0]; k++){
            cout << chunk[x][row][k] <<"  " ;//= comb[row][col];

            }
            cout <<endl;
        }
        cout <<endl<<endl;
    }

//------------------------------------------------------------------------------------------------------------------

*/
}
void    CoEvolution:: PrintSpecies()
{

	 for( int s =0; s < NoSpecies; s++){
		 Species[s].PrintPopulation();
		cout<<"species: " << s+1<<endl;
		cout<< "_____________________________"<<endl;
     }

}




void    CoEvolution:: GetBestTable(int CurrentSp)
{
  int Best;

		 for(int sN = 0; sN < CurrentSp ; sN++){
		    Best= Species[sN].BestIndex;

          // cout<<Best<<endl;
		  for(int s = 0; s < SpeciesSize[sN] ; s++)
		    	 TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];
		 }

		 for(int sN = CurrentSp; sN < NoSpecies ; sN++){
			// cout<<"g"<<endl;
			Best= Species[sN].BestIndex;
			   //cout<<Best<<" ****"<<endl;
	     for(int s = 0; s < SpeciesSize[sN] ; s++)
			TableSp[sN].SingleSp[s]= Species[sN].Population[Best].Chrome[s];
				 }

}

void   CoEvolution:: Join()
{


	int index = 0;

	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){

			//if( ((TableSp[row].SingleSp[col]/1)!=TableSp[row].SingleSp[col]))
				//cout<<"************************************error******************"<<endl;
		  Individual[index] =  TableSp[row].SingleSp[col];
		  //cout << "\n Individual: " << Individual[index];
		  index++;
		}

	  }


}




void   CoEvolution:: Print()
{



	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){
		 cout<<TableSp[row].SingleSp[col]<<" ";   }
            cout<<endl;
	  }
            cout<<endl;

            for( int row = 0; row< TotalSize ; row++)
              cout<<Individual[row]<<" ";
            cout<<endl<<endl;
}

void    CoEvolution:: EvaluateSpecies( )
{


	 for( int SpNum =0; SpNum < NoSpecies; SpNum++){

		 GetBestTable(SpNum);

//---------make the first individual in the population the best

		 for(int i=0; i < Species[SpNum].NumVariable; i++)
		  TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[0].Chrome[i];

		    Join();
		 Species[SpNum].Population[0].Fitness =  ObjectiveFunc(Individual);
		 TotalEval++;

		 Species[SpNum].BestFit = Species[SpNum].Population[0].Fitness;
		 Species[SpNum].BestIndex = 0;
		// cout<<"g"<<endl;
			 //------------do for the rest

		 for( int PIndex=0; PIndex< PopSize; PIndex++ ){


			 for(int i=0; i < Species[SpNum].NumVariable; i++)
	          TableSp[SpNum].SingleSp[i]  = Species[SpNum].Population[PIndex].Chrome[i];


		     Join();
           //  Print();

		     Species[SpNum].Population[PIndex].Fitness = ObjectiveFunc(Individual);//
		     TotalEval++;

		     if ((MINIMIZE * Species[SpNum].BestFit) > (MINIMIZE * Species[SpNum].Population[PIndex].Fitness))
		       {
		    	 Species[SpNum].BestFit = Species[SpNum].Population[PIndex].Fitness;
		    	 Species[SpNum].BestIndex = PIndex;
		    	// cout<<Species[SpNum].Population[PIndex].Fitness<<endl;
		       }

         }

			// cout<< Species[SpNum].BestIndex<<endl;
		cout<<SpNum<<" -- "<<endl;
     }







}

double   CoEvolution:: ObjectiveFunc(Layer x)
{
 double pi = acos(-1.0);
double	e = exp(1.0);

    int i,j,k;
  double fit, sumSCH;

  fit=0.0;

 if(ProbNum==1){
  // Ellipsoidal function
  for(j=0;j< x.size();j++)
    fit+=((j+1)*(x[j]*x[j]));
}


if(ProbNum==2){

 //Shifted_Sphere( int dim , double* x )
    int i;
    double z;
    double F = 0;
    for(i=0;i< x.size();i++){
        z = x[i]; //- sphere[i];
        F += z*z;
    }
    fit = F ;//+ f_bias[0];
}



 else if(ProbNum==3){
  // Schwefel's  2.22 function


	double part1 = 0.0;
	double part2 = 1.0;
	for(j=0; j< x.size(); j++){
		part1 += fabs(x[i]);
		part2 *= fabs(x[i]); }
	fit = part1+part2;


}

 else if(ProbNum==4){
  // Rosenbrock's function


 //for(j=0;j< x.size();j++)
  // fit+=((j+1)*(x[j]*x[j]));


for(j=0; j< x.size()-1; j++)
   fit += 100.0*(x[j]*x[j] - x[j+1])*(x[j]*x[j] - x[j+1]) + (x[j]-1.0)*(x[j]-1.0);
}

else if(ProbNum==5)
{

//F3 Shifted Rosenbrockâ€™s Function [âˆ’100,100]D 390
    int i;
    double z[1000];
    double F = 0;

    for(i=0;i<x.size();i++)
    z[i] = x[i] - rosenbrock[i] + 1;

    for(i=0;i<x.size()-1;i++){
        F = F + 100*( pow((pow(z[i],2)-z[i+1]) , 2) ) + pow((z[i]-1) , 2);
    }
    fit =  F + f_bias[2];
}


   else if(ProbNum==6)
   //Rastrigin
{

    double z;
    double F = 0;
    for(int i=0;i<x.size();i++){
        z = x[i];
        F += ( pow(z,2) - 10*cos(2*pi*z) );
	}
    fit = F + (10 *x.size());
}
  // cout<<fit<<endl;

   else if(ProbNum==7)
   //Shifted_Rastrigin
{
 int i;
    double z;
    double F = 0;
    for(i=0;i<x.size();i++){
        z = x[i] - rastrigin[i];
        F = F + ( pow(z,2) - 10*cos(2*pi*z) + 10);
	}
    fit = F + f_bias[3];
}
  // cout<<fit<<endl;



else if(ProbNum==8)
{
	//Shifted_Griewank
	int i;
    double z;
    double F1 = 0;
    double F2 = 1;
    for(i=0;i<x.size();i++){
        z = x[i] - griewank[i];
        F1 = F1 + ( pow(z,2) / 4000 );
        F2 = F2 * ( cos(z/sqrt(i+1)));

    }
    fit = (F1 - F2 + 1 + f_bias[4]);


}

else if(ProbNum==9){


    int i;
    double z;
    double Sum1 = 0;
    double Sum2 = 0;
    double F = 0;
    for(i=0;i<x.size();i++){
        z = x[i] - ackley[i];
        Sum1 = Sum1 + pow(z , 2 );
        Sum2 = Sum2 + cos(2*pi*z);
    }
    fit = -20*exp(-0.2*sqrt(Sum1/x.size())) -exp(Sum2/x.size()) + 20 + e + f_bias[5];

}





	//cout<<fit<<endl;




  return(fit);
}


void CoEvolution::find_parents(int s )   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

  Species[s].my_family();

  for(j=0;j<family;j++)
    {

    	  Species[s].NewPop[Species[s].kids+j].Chrome = Species[s].Population[Species[s].mom[j]].Chrome;



		 GetBestTable(s);
		 for(int i=0; i < Species[s].NumVariable; i++)
		  TableSp[s].SingleSp[i] =   Species[s].NewPop[Species[s].kids+j].Chrome[i];
		    Join();

		 Species[s].NewPop[Species[s].kids+j].Fitness = ObjectiveFunc(Individual);
		 TotalEval++;
    }
}


void CoEvolution::EvalNewPop(int pass, int s )
{

	 GetBestTable(s);
	 for(int i=0; i < Species[s].NumVariable; i++)
     TableSp[s].SingleSp[i] = 	Species[s].NewPop[pass].Chrome[i];
     Join();
  //   cout<<  Species[s].NewPop[pass].Fitness << "        pop fit"<<endl;

	 Species[s].NewPop[pass].Fitness =   ObjectiveFunc(Individual);
	 TotalEval++;


	  //   cout<<  Species[s].NewPop[pass].Fitness << "is new pop fit"<<endl;
}
void  CoEvolution::sort(int s)

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(Species[s].kids+family);i++) Species[s].list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness < dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness > dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
}


void CoEvolution::rep_parents(int s )   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {

   Species[s].Population[Species[s].mom[j]].Chrome = Species[s].NewPop[Species[s].list[j]].Chrome;

    	  GetBestTable(s);

    		 for(int i=0; i < Species[s].NumVariable; i++)
    	 TableSp[s].SingleSp[i] =   Species[s].Population[Species[s].mom[j]].Chrome[i];
    	  Join();

    	  Species[s].Population[Species[s].mom[j]].Fitness =   ObjectiveFunc(Individual);
    	  TotalEval++;
    }
}

void CoEvolution:: EvolveSubPopulations(int repetitions,double h,   double mutation,int depth, ofstream & out1)
{
double tempfit;
		int count =0;
		int tag;
		kid = KIDS;
               int numspecies =0;




	for( int s =0; s < NoSpecies; s++) {

               for (int r = 0; r < repetitions; r++){

	    		tempfit=   Species[s].Population[Species[s].BestIndex].Fitness;
	    		    Species[s].kids = KIDS;


	    	 	Species[s].RandomParents();

	 	        for(int i=0;i<	Species[s].kids;i++)
	 	   	     {
	 	   	      tag = 	Species[s].GenerateNewPCX(i, mutation, depth); //generate a child using PCX

	 	   	        if (tag == 0) {
                                   NotConverged[s]=false;
                                 //NewPop[pass].Chrome[k] end = true;
                               //   out1<<"tag1"<<endl;
                                   break;
                              }
	 	   	     }
	 	   	          if (tag == 0) {
                                       //   end = true;
                                       // out1<<"tag2"<<endl;
	 	   	        	NotConverged[s]=false;
	 	   	          }

	 	   	     for(int i=0;i<	Species[s].kids;i++)
	 	   	      EvalNewPop(i,s );


	 	      find_parents(s );  // form a pool from which a solution is to be
	 	   	                           //   replaced by the created child

	 	  	    Species[s].sort();          // sort the kids+parents by fitness
            //       sort(s);
 	 	      rep_parents(s );   // a chosen parent is replaced by the child


	 	  	   Species[s].BestIndex=0;

	 	  	         tempfit= Species[s].Population[0].Fitness;

	 	  	         for(int i=1;i<PopSize;i++)
	 	   			    if((MINIMIZE *    Species[s].Population[i].Fitness) < (MINIMIZE * tempfit))
	 	   			      {
	 	   				tempfit= Species[s].Population[i].Fitness;
	 	   			   Species[s].BestIndex=i;
	 	   			      }


               }//r

	     	}//species
}









//public GeneticAlgorithmn, public RandomNumber,


class CombinedEvolution        :    public    CoEvolution
{


       public:
    int TotalEval;
     int TotalSize;
   double Energy;
   int SAFuncEval;
     double Train;
          double Test;
           double Error;
       	 CoEvolution NeuronLevel;
    CoEvolution PD1; //Problem Decomposition 1
    CoEvolution PD2; //
 	 	 CoEvolution OneLevel;
int Cycles;
        bool Sucess;

          CombinedEvolution(){

          }

          int GetEval(){
            return TotalEval;
                  }
          int GetCycle(){
                     return Cycles;
                           }
          double GetError(){
                    return Error;
                          }

          bool GetSucess(){
                              return Sucess;
                                    }

       void   Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3,double mutation,int depth );



};

void    CombinedEvolution:: Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3,double mutation,int problem)
{


       const int culturalPopSize = 1000;
  const int CCPOPSIZE = 100; //1000

   const int maxgen =6000000; //3000000


      int NumSubPop = 0;
      int SubPopSize = 0;





   const double  MinError = 1E-20;

	 clock_t start = clock();

 Layer ErrorList(30);

 Layer IniLimitList(30);


 ErrorList[0] = MinError;//ellipsoid
 ErrorList[1] = MinError;// sphere
 ErrorList[2] = MinError; //schwel
 ErrorList[3] = MinError;//rosen
 ErrorList[4] = 390; //shifted rosen
 ErrorList[5] = MinError; //ras
 ErrorList[6] = -330; //-330 , {-5,5} shifted ras
ErrorList[7] = -180; //-180 , {-600,600} gier

 IniLimitList[0] = 5; //ellipsoid
  IniLimitList [1] = 100; //shifted sphere
 IniLimitList[2]  = 5;  //schwel
 IniLimitList[3] = 5; //rosenbrock
 IniLimitList[4] = 100; //shifted rosenbrock
 IniLimitList[5] = 5; //ras
  IniLimitList[6] = 5; //-330 , {-5,5} //shifted rastrigin
IniLimitList[7] = 600; //-180 , {-600,600} //greiark


           TotalEval = 0;
          Sucess= false;

                 Cycles =0;





//cout<<" Evaluated OneLevel ----------->"<<endl;
//-----------------------------------------------------------

//PD1: NumberSP

      NumSubPop = 10;

     SubPopSize = 100 ;

    // int NumSubPop2 = 2;

     //int SubPopSize2 = 4;

     Sizes arbitrary;
arbitrary.push_back(10);
arbitrary.push_back(12);
arbitrary.push_back(8);
arbitrary.push_back(9);
arbitrary.push_back(8);
arbitrary.push_back(13);
arbitrary.push_back(14);
arbitrary.push_back(6);
arbitrary.push_back(13);
arbitrary.push_back(7);


 for(int n = 0; n < NumSubPop ; n++)
		  PD1.SpeciesSize.push_back(SubPopSize);

         PD1.ProbNum = (int)h;
	   PD1.NoSpecies = NumSubPop;
       PD1.InitializeSpecies(CCPOPSIZE, IniLimitList[h-1]);

        PD1.EvaluateSpecies();

       for( int s =0; s < NeuronLevel.NoSpecies; s++)
    	  PD1.NotConverged[s]=true;

        //---------------
       // PD1.PrintSpecies();

       // PD1.InitialiseBoth();

        NumSubPop = 50;
        SubPopSize = 20;

       Sizes arbitrary2;

       /*arbitrary2.push_back(10);
       arbitrary2.push_back(12);
       arbitrary2.push_back(8);
       arbitrary2.push_back(9);
       arbitrary2.push_back(11);
       arbitrary2.push_back(13);
       arbitrary2.push_back(7);
       arbitrary2.push_back(12);
       arbitrary2.push_back(9);
       arbitrary2.push_back(9);  */

       arbitrary2.push_back(8);
       arbitrary2.push_back(12);
       arbitrary2.push_back(8);
       arbitrary2.push_back(9);
       arbitrary2.push_back(8);
       arbitrary2.push_back(15);
       arbitrary2.push_back(7);
       arbitrary2.push_back(11);
       arbitrary2.push_back(9);
       arbitrary2.push_back(11);



// initialise randomly
        for(int n = 0; n < NumSubPop ; n++)
		  PD2.SpeciesSize.push_back(SubPopSize);
		  PD2.ProbNum = (int)h;
	   PD2.NoSpecies = NumSubPop;
     PD2.InitializeSpecies(CCPOPSIZE, IniLimitList[h-1]);



// +++++++++++++++++++++++++++++++++++++  COPY AND USE SAME RANDOM SEARCH SPACE ++++++++++++++++++++++++++++++++++++++++++++++++++++
        vector <vector<double> > combine;
        for (int i=0; i<CCPOPSIZE; i++){


            vector<double> row;
            combine.push_back(row);

            }

// copy ----------------------------------------------------------------------------------------------------
                        for (int sp=0; sp < PD1.NoSpecies; sp++){

      	                 for( int row = 0; row< PD1.PopSize ; row++) {

                            for( int col = 0; col < PD1.SpeciesSize[sp] ; col++) {   //totalsize

                                combine[row].push_back(PD1.Species[sp].Population[row].Chrome[col]);

                            }


                        }

            }


/*

cout << " \n \ntest copied [1][7] -> " << combine[1][7] << endl;

                        for( int row = 0; row< CCPOPSIZE ; row++) {

                            for( int col = 0; col < PD1.TotalSize ; col++) {   //totalsize

                                cout << combine[row][col] << "  ";


                            }


                        }

*/

//-----------------------------------------------------------------------------------------------------------------------

 // split into the problem decomposition definition of PD2
/*
 cout << "\n ---- pre split" ;

double chunk [PD2.TotalSize/PD2.NoSpecies][CCPOPSIZE][PD2.SpeciesSize[0]];

for (int sp=0; sp < PD2.NoSpecies; sp++){
    for (int row = 0; row < CCPOPSIZE; row++){

        for (int col=0; col<PD2.TotalSize; col++){
            chunk[col/PD2.SpeciesSize[sp]][row][col%PD2.SpeciesSize[sp]] = combine[row][col];

            }

        }
 }


cout << "\n ---- post split \n" ;   */
/*
cout << "\n\n after splitting \n\n";
    for (int x=0; x<PD1.TotalSize/PD1.SpeciesSize[0]; x++){
        for (int row = 0; row < CCPOPSIZE; row++){

            for (int k=0; k< PD1.SpeciesSize[0]; k++){
                cout << chunk[x][row][k] <<"  " ;

                }
                cout <<endl;
            }
            cout <<endl<<endl;
        }

cout << chunk[0][1][1] <<endl <<endl;

*/

/*
// --- Vectors can also be used just as Chunk Array
vector< vector< vector<double> > > vI3Matrix(PD1.TotalSize/PD2.SpeciesSize[0], vector< vector<double> > (CCPOPSIZE, vector<double>(2)) );

    for (int sp=0; sp < PD2.NoSpecies; sp++){
        for (int row = 0; row < CCPOPSIZE; row++){
	         for (int col=0; col<PD2.TotalSize; col++){

	            vI3Matrix[col/PD2.SpeciesSize[sp]][row][col%PD2.SpeciesSize[sp]] = combine[row][col];

	         }

	      }

	   }

	   */

/* for (int sp=0; sp < PD1.TotalSize/PD1.SpeciesSize[0]; sp++){
        for (int row = 0; row < CCPOPSIZE; row++){
	         for (int col=0; col<PD1.SpeciesSize[0]; col++){

	            cout << vI3Matrix[sp][row][col] << "  ";
	         }
	         cout <<endl;
	      }
	      cout <<endl <<endl;
	   }
	   cout << vI3Matrix.size();
  */

/*
// now update the values of PD2 which was initially initialised randomly.
                      for(int sp = 0; sp < PD2.NoSpecies  ; sp++){
                        for (int row =0; row < CCPOPSIZE ; row ++){
      	                 for(int col = 0; col <  PD2.SpeciesSize[sp] ; col++) {
                     PD2.Species[sp].Population[row].Chrome[col]= chunk[sp][row][col];
                                             }
                                            }
                                          }

                  //  PD2.PrintSpecies();  // it has same population now...   */



//++++++++++++++++++++++++++++++++++++++++++++++ COPIED AND INITIALIZED WITH SAME RANDOM SEARCH SPACE +++++++++++++++++++++++++++++++

        PD2.EvaluateSpecies();

       for( int s =0; s < NeuronLevel.NoSpecies; s++)
    	  PD2.NotConverged[s]=true;





TotalEval=0;

 PD1.TotalEval=0;
      PD2.TotalEval=0;
     //  PD3.TotalEval=0;



int count =0;

      	             //----------------------------------------------------


Layer BestMemes;

Layer ErrorArray;


       double BestPD1, BestPD2, BestPD3;

                                       // int  m =0;


        while(TotalEval<=maxgen){

		//	while(true){






   // PD 1   ----------------------------

		                     for( int cycle = 0; cycle < 50; cycle++){
      	                   PD1.EvolveSubPopulations(1,1, 0,0, out2);
      	                 //  PD1.GetBestTable();
      	                  PD1.Join();
      	                  int bestindex =  PD1.Species[PD1.NoSpecies-1].BestIndex;
                          Error= PD1.Species[PD1.NoSpecies-1].Population[bestindex].Fitness;


	   }

	                BestPD1 = Error;



   // PD 2  ----------------------------

		                     for( int cycle = 0; cycle < 50; cycle++){
      	                   PD2.EvolveSubPopulations(1,1, 0,0, out2);
      	                  PD2.Join();
      	                  int bestindex =  PD2.Species[PD2.NoSpecies-1].BestIndex;
                          Error= PD2.Species[PD2.NoSpecies-1].Population[bestindex].Fitness;


	                          }

	                           BestPD2 = Error;

		                TotalEval= PD2.TotalEval  +  PD1.TotalEval;

           if(TotalEval%5000==0){
          // out1<<  Error <<"   "<<TotalEval<<endl;


		      // cout<<Error<<" "<<TotalEval<<endl;

                // BestMemes.push_back(Error);

         }

               //  TotalEval =   PD1.TotalEval;

/*

      	                if( ErrorList[h-1] == MinError){ //ways of stopping
      	                if( Error < 1E-20){
                         Sucess =true;
                        //break;

                        }   }

                       else if( ErrorList[h-1] <0){
						    if(((Error-ErrorList[h-1]))< 1E-10){

                         Sucess =true;
                        // break;
					   }  }
                        else{
      	               if(((fabs(Error)-fabs(ErrorList[h-1])))< 1E-10){  //way of stopping for some problems
                         Sucess =true;
                        //break;

                        }   }



                     if((TotalEval>  maxgen/2)&& (  BestMemes[BestMemes.size()-1] == BestMemes[BestMemes.size()-3])){ //make sure that if there is no change in 20 k FE, then break

		             	Sucess = false;
                         //break;
                           }
*/
                        //---------------------------------


                             if( (BestPD2 > BestPD1)){

                             out1<<"PD1      "<< BestPD1<< "  "<<  PD1.TotalEval <<"  " << TotalEval << endl;


                                 int  m =0;
                    for(int sp = 0; sp < PD2.NoSpecies  ; sp++){
      	                 for(int col = 0; col <  PD2.SpeciesSize[sp] ; col++) {
                     PD2.Species[sp].Population[PD2.Species[PD2.NoSpecies-1].BestIndex].Chrome[col]= PD1.Individual[m];
                    PD2.Species[sp].Population[PD2.Species[PD2.NoSpecies-1].BestIndex].Fitness = BestPD1;
                        PD2.Species[PD2.NoSpecies-1].BestIndex = PD1.Species[PD1.NoSpecies-1].BestIndex;
                                         m++;
                                             }
                                          }
                           int nltemp = PD2.TotalEval ;

                          PD2.EvaluateSpecies();
                           PD2.TotalEval =  nltemp ;

      	                PD2.TotalEval=nltemp + ((CCPOPSIZE)* PD2.NoSpecies );
									  }

									  else{

								out1<<" PD2     "<< BestPD2<<"   "<<PD2.TotalEval << "  " << TotalEval <<endl;
								//  out1<<BestPD2<<endl;
                               int  m =0;
                    for(int sp = 0; sp < PD1.NoSpecies  ; sp++){
      	                 for(int col = 0; col <  PD1.SpeciesSize[sp] ; col++) {
                     PD1.Species[sp].Population[PD1.Species[PD1.NoSpecies-1].BestIndex].Chrome[col]= PD2.Individual[m];
                    PD1.Species[sp].Population[PD1.Species[PD1.NoSpecies-1].BestIndex].Fitness = BestPD2;
                        PD1.Species[PD1.NoSpecies-1].BestIndex = PD2.Species[PD2.NoSpecies-1].BestIndex;
                                         m++;
                                             }
										  }

										    int temp = PD1.TotalEval ;

                          PD1.EvaluateSpecies();
                           PD1.TotalEval =  temp ;

      	                PD1.TotalEval=temp  + ((CCPOPSIZE)* PD1.NoSpecies );


									  }


      	               	    count++;

      	               	       }







                out1 <<endl <<endl <<endl<<endl<< " ++++++++++++++++++++++++++++++++++++ " << endl<<endl<<endl<<endl<<endl;


                 out2<<Error<< "   " << TotalEval <<endl;


}







//---------------------------------------------------------------------------------------
int main(void)
{


	ofstream out1;
		out1.open("Oneout1.txt");
	ofstream out2;
	     out2.open("Oneout2.txt");
	 	ofstream out3;
	 	     out3.open("Out3_100_PD2_PD3.txt");
ofstream out4;
	 	     out4.open("Out4_100_PD2_PD3.txt");

    FILE * pFile;
     pFile = fopen ("Output3all.txt","a");


/*F1 Shifted Sphere Function [âˆ’100,100]D âˆ’450
F2 Shifted Schwefelâ€™s Problem 2.21 [âˆ’100,100]D âˆ’450
F3 Shifted Rosenbrockâ€™s Function [âˆ’100,100]D 390
F4 Shifted Rastriginâ€™s Function [âˆ’5,5]D âˆ’330
F5 Shifted Griewankâ€™s Function [âˆ’600,600]D âˆ’180
F6 Shifted Ackleyâ€™s Function [âˆ’32,32]D âˆ’140
F7 Schwefelâ€™s Problem 2.22 [âˆ’10,10]D 0
F8 Schwefelâ€™s Problem 1.2 [âˆ’65.536,65.536]D 0
F9 Extended f10 [âˆ’100,100]D 0
F10 Bohachevsky [âˆ’15,15]D 0
F11 Schaï¬€er [âˆ’100,100]D 0
 */


 struct timeval start, end;
   double secs_used,milli_used;




     for(double hidden=2;hidden<=2;hidden++){
    	 for(double onelevelstop=1;onelevelstop<= 1; onelevelstop++){
    	 Sizes EvalAverage;
    	 Layer ErrorAverage;
    	Layer CycleAverage;

    	 int MeanEval=0;
    	 double MeanError=0;
    	double MeanCycle=0;

    	 int EvalSum=0;
    	 double ErrorSum=0;
    	double CycleSum=0;
    	double maxrun = 3;

    	double MeanTime =0;
    	double TotalTime = 0;
int success =0;
    	 for(int run=1;run<=maxrun;run++){
    	  CombinedEvolution Combined;
    	      gettimeofday(&start, NULL);
          Combined.Procedure(false, hidden, out1, out2, out3, 0  , onelevelstop );
     gettimeofday(&end, NULL);



    secs_used=(end.tv_sec - start.tv_sec);
    TotalTime += secs_used;

if(Combined.GetSucess()){
   success++;






          }
  ErrorAverage.push_back(Combined.GetError());
    	  MeanError+= Combined.GetError();

    	  CycleAverage.push_back(Combined.GetCycle() );
    	      	  MeanCycle+= Combined.GetCycle();


          EvalAverage.push_back(Combined.GetEval());
          MeanEval+=Combined.GetEval();

    	               }//run

    	               MeanTime = TotalTime/maxrun;

    	              TotalTime  = 0;

    	 MeanEval=MeanEval/EvalAverage.size();
    	 MeanError=MeanError/ErrorAverage.size();
    	 MeanCycle=MeanCycle/CycleAverage.size();

    	  for(int a=0; a < EvalAverage.size();a++)
    	EvalSum +=	(EvalAverage[a]-MeanEval)*(EvalAverage[a]-MeanEval);

    	  EvalSum=EvalSum/ EvalAverage.size();
    	  EvalSum = sqrt(EvalSum);

    	  EvalSum = 1.96*(EvalSum/sqrt( EvalAverage.size()));
    	  for(int a=0; a < CycleAverage.size();a++)
    		  CycleSum +=	(CycleAverage[a]-MeanCycle)*(CycleAverage[a]-MeanCycle);

    	  CycleSum=CycleSum/ CycleAverage.size();
    	  CycleSum = sqrt(CycleSum);
    	  CycleSum = 1.96*(CycleSum/sqrt( CycleAverage.size()));
    	  for(int a=0; a < ErrorAverage.size();a++)
        ErrorSum +=	(ErrorAverage[a]-MeanError)*(ErrorAverage[a]-MeanError);

    	 ErrorSum=ErrorSum/ ErrorAverage.size();
    	ErrorSum = sqrt(ErrorSum);
        ErrorSum = 1.96*(ErrorSum/sqrt(ErrorAverage.size()) );
 	out3<<onelevelstop<<"   "<<hidden<<"  "<<MeanEval<<"  "<<EvalSum<<"  "<<MeanError<<"  "<<ErrorSum<<"  "<<MeanCycle<<"  "<<CycleSum<<"  "<<success<<"  --- "<<MeanTime<<endl;


 	out4<<onelevelstop<<" & "<<hidden<<" &  "<<MeanEval<<" $\pm$  "<<EvalSum<<" & "<<MeanError<<"  $\pm$  "<<ErrorSum<<" &  "<<success<<"  \\ "<<endl;

     	EvalAverage.empty();
     	ErrorAverage.empty();
     	CycleAverage.empty();
    	 }//onelevelstop
     }//hidden
     out3<<"\\hline"<<endl;

fprintf ( pFile, " \n -----------------  \n");

    fclose (pFile);
	out1.close();
	out2.close();
	out3.close();
out4.close();


 return 0;

};


